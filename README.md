## 👨🏻‍💻 Sobre o projeto

- O sistema web consiste em uma landing page de apresentação do jogo Transistor e seus personagens.


## 💻 Iniciando o projeto

**Clone o projeto e acesse a pasta gerada**

```bash
$ git clone https://pedrogomes1@bitbucket.org/pedrogomes1/sorocabacom.git
$ cd sorocabacom
```

**Siga os passos abaixo**

```bash
# Instale todas as dependências
$ yarn

# Inicie a aplicação
$ yarn start
```

Feito por Pedro Gomes 👋 [Meu Linkedin](https://www.linkedin.com/in/pedro-henrique-gomes-barbosa-667766178/)
