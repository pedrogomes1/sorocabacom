import styled from 'styled-components';

export const Container = styled.header`
  width: 100%;
  height: 10.9rem;
  background-color: #363636;

  display: flex;
  justify-content: center;
  align-items: center;

  img {
    max-height: 10.9rem;
  }

  span {
    font-family: 'Montserrat', sans-serif;
    font-size: 2.3rem;
    color: #ffffff;
    margin-left: 3rem;
  }

  @media (max-width: 400px) {
    span {
      font-size: 1.6rem;
      margin-left: 0;
    }
  }
`;
