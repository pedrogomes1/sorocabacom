import React from 'react';

import { Container } from './styles';

import IconHeader from '../../assets/images/iconHeader.png';

const Header: React.FC = () => {
  return (
    <Container>
      <img src={IconHeader} alt="" />
      <span>SUPERGIANTGAMES</span>
    </Container>
  );
};

export default Header;
