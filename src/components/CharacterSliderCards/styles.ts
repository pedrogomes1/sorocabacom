import styled from 'styled-components';

export const Container = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;

  min-height: 114.7rem;
  height: 100%;
  padding: 30rem 10.8rem;

  .arrow-prev {
    margin-right: 10rem;
    cursor: pointer;
    transition: transform 0.5s;

    :hover {
      transform: translateX(-5px);
    }
  }

  .arrow-next {
    margin-left: 10rem;
    cursor: pointer;
    transition: transform 0.5s;

    :hover {
      transform: translateX(5px);
    }
  }

  @media (max-width: 1840px) {
    .arrow-prev {
      margin-right: 4rem;
    }

    .arrow-next {
      margin-left: 4rem;
    }
  }

  @media (max-width: 1740px) {
    .arrow-prev {
      margin-right: 2rem;
    }

    .arrow-next {
      margin-left: 2rem;
    }
  }
  @media (max-width: 685px) {
    display: flex;
    justify-content: center;

    > img {
      width: 2.5rem;
      height: 2.5rem;
    }

    padding: 30rem 2rem;
  }

  @media (max-width: 385px) {
    img {
      margin: 0.5rem !important;
    }
  }
`;

export const WrapperCharacters = styled.div`
  display: grid;
  grid-template-columns: repeat(3, 1fr);

  justify-items: center;

  div:nth-child(2) {
    margin: 0 10rem;
  }

  @media (max-width: 1695px) {
    grid-template-columns: repeat(2, 1fr);
    grid-gap: 22rem;
    justify-items: center;

    div:nth-child(2) {
      margin: 0;
    }
  }

  @media (max-width: 1275px) {
    grid-template-columns: 1fr;
  }
`;

export const Card = styled.div`
  width: 38.7rem;
  height: 61.3rem;

  background: #ffffff;
  box-shadow: 0px 3px 6px #ffffff63;
  border-radius: 2.7rem;

  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;

  @media (max-width: 490px) {
    width: 26.7rem;
    height: auto;

    p {
      padding: 2rem;
    }
  }

  p {
    font-size: 2rem;
    max-width: 32.6rem;
    text-align: left;
    font-family: 'Open Sans', sans-serif;
    color: #363636;
    margin: auto;

    @media (max-height: 764px) {
      font-size: 1.5rem;
      width: 26rem;
    }
  }

  div {
    width: 36.4rem;
    height: 44rem;
    background: #363636;
    border: 3px solid #ffffff;
    border-radius: 86px;
    margin-top: -9rem;

    display: flex;
    justify-content: center;
    align-items: center;

    img {
      margin-bottom: 8rem;

      @media (max-width: 490px) {
        width: 23rem;
        height: 35rem;
      }

      @media (max-width: 385px) {
        width: 20rem;
        height: 30rem;
      }
    }

    .persons-image {
      @media (max-height: 764px) {
        margin-bottom: 8rem;
      }
    }

    @media (max-width: 490px) {
      width: 20.8rem;
      height: 30.8rem;
    }

    @media (max-width: 385px) {
      margin-top: -6rem;
    }
  }
`;
