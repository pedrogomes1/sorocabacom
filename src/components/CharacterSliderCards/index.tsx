import React from 'react';
import grant from '../../assets/images/personagem-grant.png';
import red from '../../assets/images/personagem-red.png';
import sybil from '../../assets/images/personagem-sybil.png';
import prev from '../../assets/images/prev.svg';
import next from '../../assets/images/next.svg';

import { Container, WrapperCharacters, Card } from './styles';

const CharacterSliderCards: React.FC = () => {
  return (
    <Container>
      <img src={prev} alt="" className="arrow-prev" />
      <WrapperCharacters>
        <Card>
          <div>
            <img src={grant} alt="" />
          </div>

          <p>
            A Camerata foi apenas os dois no início, e suas fileiras nunca foram
            destinadas a exceder um número a ser contado em uma mão.
          </p>
        </Card>

        <Card>
          <div>
            <img src={red} alt="" className="persons-image" />
          </div>

          <p>
            Red, uma jovem cantora, entrou em posse do Transistor. Sendo a
            poderosa espada falante. O grupo Possessores quer tanto ela quanto o
            Transistor e está perseguindo implacavelmente a sua procura.
          </p>
        </Card>

        <Card>
          <div>
            <img src={sybil} alt="" className="persons-image" />
          </div>

          <p>
            Sybil é descrita pelo Transistor como sendo os &quot;olhos e
            ouvidos&quot; da Camerata.
          </p>
        </Card>
      </WrapperCharacters>
      <img src={next} alt="" className="arrow-next" />
    </Container>
  );
};

export default CharacterSliderCards;
