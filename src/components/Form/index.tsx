import React from 'react';
import * as Yup from 'yup';
import { yupResolver } from '@hookform/resolvers/yup';
import { useForm } from 'react-hook-form';

import { Container } from './styles';

interface IFormDataProps {
  name: string;
  email: string;
  message: string;
}

const schema = Yup.object().shape({
  name: Yup.string().required('Campo obrigatório!'),
  email: Yup.string()
    .email('E-mail no formato incorreto')
    .required('Campo obrigatório!'),
  message: Yup.string().required('Campo obrigatório'),
});

const Forms: React.FC = () => {
  const { handleSubmit, errors } = useForm({
    resolver: yupResolver(schema),
  });

  const onSubmit = (data: IFormDataProps) => {
    return data;
  };

  return (
    <Container>
      <h1>Formulário</h1>

      <p>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim
        veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea
        commodo consequat.
      </p>

      <form onSubmit={handleSubmit(onSubmit)}>
        <div className="wrapper-input">
          <input type="text" name="name" placeholder="Nome" />
          <span>{errors.name?.message}</span>
        </div>

        <div className="wrapper-input">
          <input type="email" name="email" placeholder="E-mail" />
          <span>{errors.email?.message}</span>
        </div>

        <div className="wrapper-textarea">
          <textarea name="message" placeholder="Mensagem" />
          <span>{errors.message?.message}</span>
        </div>
        <button type="submit">Enviar</button>
      </form>
    </Container>
  );
};

export default Forms;
