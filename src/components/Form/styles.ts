import styled from 'styled-components';

export const Container = styled.div`
  width: 100%;

  max-width: 108.2rem;
  height: 83.2rem;

  margin: -5rem 3rem 0;

  box-shadow: 0px 0px 6px #0000004d;
  border-radius: 1rem;
  background: #ffffff;

  padding: 8.2rem 3rem 0;

  text-align: center;

  h1 {
    font-size: 4.6rem;
    font-family: 'Montserrat', sans-serif;
    color: #63c7a9;
    text-transform: uppercase;

    @media (max-width: 390px) {
      font-size: 3rem;
    }
  }

  p {
    max-width: 74.6rem;
    font-family: 'Open Sans', sans-serif;
    font-size: 2rem;
    text-align: left;
    margin: 4.1rem auto 0;

    @media (max-width: 390px) {
      font-size: 1.4rem;
    }
  }

  form {
    display: grid;
    grid-template-areas: 'col1 col2' 'col3 col3';

    width: 100%;
    max-width: 52.8rem;

    margin: 5.1rem auto 0;

    font-family: 'Open Sans', sans-serif;

    input {
      width: 100%;
      height: 4.8rem;
      border: 1px solid #363636;

      padding: 1.5rem;

      font-size: 1.8rem;
      font-family: 'Open Sans', sans-serif;

      @media (max-width: 587px) {
        margin-top: 3rem !important;
      }
    }

    .wrapper-input {
      display: flex;
      flex-direction: column;

      grid-area: col1 col2;
    }

    .wrapper-textarea {
      max-width: 52.8rem;
      flex-direction: column;
      grid-area: col3;
    }

    div:first-child {
      margin-right: 3.04rem;
    }

    @media (max-width: 500px) {
      display: flex;
      flex-direction: column;

      margin-top: 0.5rem;

      .wrapper-input {
        margin: 0 !important;
      }
      textarea {
        height: 12.7rem !important;
      }
    }
  }

  textarea {
    width: 100% !important;
    height: 19.7rem !important;
    font-size: 1.8rem;
    padding: 1.5rem;
    margin-top: 3.9rem;

    @media (max-width: 587px) {
      margin-top: 3rem;
    }
  }

  span {
    color: #f64c75;
    font-size: 1.4rem;
    font-weight: bold;
  }

  button {
    width: 25rem;
    height: 4.8rem;
    background: #63c7a9;

    justify-content: flex-start;

    font-size: 1.8rem;
    font-family: 'Open Sans', sans-serif;

    color: #ffffff;
    text-transform: uppercase;
    border: 0;

    margin-top: 4.87rem;

    transition: opacity 0.3s;
  }

  button:hover {
    opacity: 0.8;
  }
`;
