import React, { useCallback } from 'react';
import up from '../../assets/images/up.svg';
import { Container, StartPageButton } from './styles';

const Footer: React.FC = () => {
  const handleTopPage = useCallback(() => {
    window.scrollTo(0, 0);
  }, []);

  return (
    <Container>
      <StartPageButton onClick={handleTopPage}>
        <img src={up} alt="" />
      </StartPageButton>
    </Container>
  );
};

export default Footer;
