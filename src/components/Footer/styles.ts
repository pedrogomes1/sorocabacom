import styled, { keyframes } from 'styled-components';

export const Container = styled.footer`
  height: 41rem;

  display: flex;
  justify-content: flex-end;
  align-items: center;
`;

const animationArrowUp = keyframes`
  from {
    transform: translateY(0px);
  }

  to {
    transform: translateY(-5px);
  }
`;

export const StartPageButton = styled.div`
  width: 10.7rem;
  height: 10.7rem;

  cursor: pointer;

  border-radius: 50%;

  background: #ffffff;

  margin-right: 14.3rem;

  display: flex;
  justify-content: center;
  align-items: center;
  transition: background-color 0.3s;

  animation: ${animationArrowUp} 1s ease-in-out infinite alternate;

  :hover {
    background-color: #f5f5f5;
  }

  @media (max-width: 646px) {
    margin: 0 auto;
  }
`;
