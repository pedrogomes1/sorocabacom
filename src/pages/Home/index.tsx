import React from 'react';
import imageBackground from '../../assets/images/image-main.png';
import fundoAvatar from '../../assets/images/fundo-avatar.png';
import feather from '../../assets/images/pena-1.png';
import feather2 from '../../assets/images/pena-2.png';
import feather3 from '../../assets/images/pena-3.png';
import feather4 from '../../assets/images/pena-4.png';
import avatar from '../../assets/images/avatar.png';
import down from '../../assets/images/down.svg';
import Header from '../../components/Header';
import Form from '../../components/Form';
import Footer from '../../components/Footer';
import CharacterSliderCards from '../../components/CharacterSliderCards';

import {
  Container,
  Section,
  WrapperAvatar,
  ScrollLow,
  SectionForm,
} from './styles';

const Home: React.FC = () => {
  return (
    <Container>
      <Header />

      <Section src={imageBackground}>
        <div>
          <WrapperAvatar src={fundoAvatar}>
            <span>TRANSISTOR - RED THE SINGER</span>
            <img src={avatar} alt="" className="avatar" />
            <img className="feather" src={feather} alt="" />
            <img className="feather2" src={feather2} alt="" />
            <img className="feather3" src={feather3} alt="" />
            <img className="feather4" src={feather4} alt="" />

            <p>
              &quot;Olha, o que quer que você esteja pensando, me faça um favor,
              não solte.&quot;
            </p>
          </WrapperAvatar>

          <ScrollLow>
            <div className="mouse" />
            <img src={down} alt="" />
          </ScrollLow>
        </div>
      </Section>

      <CharacterSliderCards />

      <SectionForm>
        <Form />
      </SectionForm>

      <Footer />
    </Container>
  );
};

export default Home;
