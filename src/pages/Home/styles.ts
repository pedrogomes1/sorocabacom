import styled from 'styled-components';

interface SectionProps {
  src: string;
}

interface WrapperAvatarProps {
  src: string;
}

export const Container = styled.div`
  min-height: 100vh;
  height: 100%;
  background-color: #363636;
`;

export const Section = styled.section<SectionProps>`
  background-image: url(${({ src }) => src});
  background-repeat: no-repeat;
  background-size: cover;
  background-position: 50% 50%;
  min-height: 100vh;

  > div {
    position: relative;
    display: flex;
    justify-content: center;
  }
`;

export const WrapperAvatar = styled.div<WrapperAvatarProps>`
  background-image: url(${({ src }) => src});
  background-size: cover;
  height: 100.8rem;
  width: 58.6rem;

  position: relative;

  display: flex;
  align-items: center;
  justify-content: center;

  @media (max-height: 764px) {
    width: 40rem;
  }

  @media (max-width: 530px) {
    .avatar {
      width: 350px;
    }
  }

  @media (max-width: 480px) {
    .avatar {
      width: 300px;
      height: 400px;
    }
  }

  img {
    position: absolute;

    max-width: 52.6rem;
    height: 75.4rem;

    @media (max-height: 764px) {
      height: 50rem;
    }
  }

  span {
    font-family: 'Montserrat', sans-serif;
    font-size: 1.5rem;
    color: #ffffff;
    position: absolute;
    top: 1.3rem;
    left: 4rem;
  }

  .feather {
    width: 9.6rem;
    height: 10rem;

    position: absolute;
    left: 2rem;
    bottom: 40%;

    @media (max-height: 764px) {
      left: 5%;
      bottom: 35%;
    }
  }

  .feather2 {
    width: 13rem;
    height: 12rem;
    position: absolute;
    top: 40%;
    left: -4rem;

    @media (max-height: 764px) {
      left: -10%;
      top: 35%;
    }

    @media (max-width: 480px) {
      left: 0;
    }
  }

  .feather3 {
    position: absolute;
    width: 18rem;
    height: 15rem;
    right: -6%;

    @media (max-width: 660px) {
      width: 14rem;
      height: 14rem;

      right: 0;
    }

    @media (max-width: 582px) {
      width: 10rem;
      height: 10rem;

      right: 3%;
    }
  }

  .feather4 {
    position: absolute;
    width: 15.7rem;
    height: 15.5rem;
    right: -9rem;
    bottom: 30%;

    @media (max-width: 764px) {
      width: 12rem;
      height: 12rem;

      right: 2rem;
      bottom: 32%;
    }
  }

  p {
    position: absolute;
    font-family: 'Open Sans', sans-serif;
    color: #f0f0f2;
    text-shadow: 0px 0.3rem 0.6rem #00000029;
    font-size: 2rem;
    max-width: 27rem;
    text-align: center;

    @media (max-height: 1500px) {
      bottom: 1%;
    }
  }
`;

export const ScrollLow = styled.div`
  position: absolute;
  bottom: -7rem;
  display: flex;
  justify-content: flex-end;
  flex-direction: column;
  align-items: center;

  .mouse {
    width: 2.9rem;
    height: 4.8rem;
    background: #ffffff 0% 0%;
    box-shadow: 0px 3px 6px #00000029;
    border-radius: 124px;
    box-shadow: 0px 3px 6px #00000029;
    position: relative;

    cursor: pointer;

    ::before {
      content: '.';
      position: absolute;
      top: 0.9rem;
      left: 1.25rem;

      border-radius: 20px;
      width: 0.4rem;
      height: 1rem;

      background: #000;
    }
  }
`;

export const SectionForm = styled.div`
  height: 74.2rem;
  background: transparent linear-gradient(143deg, #7dede2 0%, #58b790 100%);

  display: flex;
  justify-content: center;
`;
